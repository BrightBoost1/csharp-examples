1. create a repo in gitlab for your C# exercises
2. add the readme in gitlab
3. clone to VS 2019/2022
4. Add a file called new.txt
5. Commit the changes
6. Push to gitlab
7. Add a file to the repo via gitlab
8. Pull this file in VS 2019/2022

9. create a new branch
10. make some changes to the branch
11. push it to gitlab
12. verify your new branch is on gitlab
13. create a merge conflict by modifying the same file and then merging
14. solving merge conflict
